# Taller 3 

##Objectivo

`Acoplar nuestras respuestas al repositorio principal`


##Caso 1 - comenzamos desde 0

- realizamos fork al repositorio : https://gitlab.com/jacgg/taller3_sesion_git.git
- clonamos nuestro fork en nuestro local
- en la raiz de nuestro proyecto clonado via  terminal creamos nuestra rama "final_jguerrag"
- una vez verifiquemos que estamos en nuestra rama nos dirigimos a la carpeta de nuestro proyecto
- creamos un folder con nuestro username y dentro pegamos nuestras respuestas del test.
- Se adminte [txt]


##Caso 2 - repositorio ya clonado

- Desde consola en nuestro repositorio ya clonado agregamos un nuevo origen 
`git remote add original https://gitlab.com/jacgg/taller3_sesion_git.git`
- Actualizamos nuestra rama master.
`git checkout master`
`git pull original master`
- creamos nuestra rama , reemplazar por su username 
`git checkout -b final_jguerrag`  
- una vez verifiquemos que estamos en nuestra rama nos dirigimos a la carpeta de nuestro proyecto
- creamos un folder con nuestro username y dentro pegamos nuestras respuestas del test.
- Se adminte [txt]




## Merge-request
- publicamos nuestra rama a git lab
`git push origin final_jguerrag` 
- Desde GitLab realizamos el MergeReques.


      

##OBs:
- las preguntas estan en el repositorio



 
